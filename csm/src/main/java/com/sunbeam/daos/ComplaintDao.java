package com.sunbeam.daos;



import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.Complaint;




public interface ComplaintDao extends JpaRepository<Complaint , Integer>{

	
	
}
