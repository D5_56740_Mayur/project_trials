package com.sunbeam.daos;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.Shipment;

public interface ShipmentDao extends JpaRepository<Shipment, Integer>{
	
	Shipment findByshipmentId (int id);
	//List<Blog> findByUserId(int id);
  //List<Shipment> findByperson(int id);
	Shipment findByShipmentId(int id);
//	@Query("UPDATE Blog b SET b.state = ?2 WHERE b.id = ?1")
//	int updateState(int id, int state);
//	@Query("UPDATE Shipment b SET b.state = ?2 WHERE b.id = ?1")
//	String updateState (int id, int state);

}
