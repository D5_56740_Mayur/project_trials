package com.sunbeam.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.PersonDto;
import com.sunbeam.dtos.Response;
import com.sunbeam.dtos.ShipmentDto;
import com.sunbeam.entities.Complaint;
import com.sunbeam.entities.Shipment;
import com.sunbeam.services.PersonServiceImpl;

@CrossOrigin(origins = "*")
@RequestMapping("v1")
@RestController
public class UserControllerImpl {
	@Autowired
	private PersonServiceImpl personService;

//	@GetMapping("/home")
//	public User get() {
//		return userService.findByUserId(1);
//	}
	@GetMapping("/complaints")
	public ResponseEntity<?> findComplaints(){
		List<Complaint> result = new ArrayList<>();
		
		result=personService.findAllComplaints();
		return  Response.success(result);
	}
	@GetMapping("/Person/{id}")
	public ResponseEntity<?> findPersonById(@PathVariable("id") int id){
		//List<Person> result=new ArrayList<>();
		PersonDto result1=personService.findPersonById(id);
		return Response.success(result1);
		
	}
	@GetMapping("/Person1/{name}")
	public ResponseEntity<?> findPersonByNmae(@PathVariable("name") String name){
		//List<Person> result=new ArrayList<>();
		PersonDto result2=personService.findPersonByName(name);
		return Response.success(result2);
		
	}
//	@GetMapping("/blog/details/{id}")
//	public ResponseEntity<?> findBlogById(@PathVariable("id") int id) {
//		BlogDTO result = blogService.findBlogById(id);
//		return Response.success(result);
//	}
//	@GetMapping("/shipment/{id}")
//	public ResponseEntity<?> findShipments(@PathVariable("id") int id)
//	{
//		List<Shipment> result3 = new ArrayList<>();
//		result3=personService.findAllShipments(id);
//		return Response.success(result3);
//		
//		
//	}
//	@PutMapping("/modify/{id}")
//	public ResponseEntity<?> modifyEmployeeDetails(@PathVariable("id") int id , @RequestBody EmployeeDto employeeDto){
//		Map<String , Object> result = personService.editDetails(id, employeeDto);
//		return Response.success(result);
//	}
//	@PutMapping("/update/{id}")
//	public ResponseEntity<?> modifyShipmentDetails(@PathVariable("id") int shipmentId , @RequestBody Shipment shipment)
//	{
//		Map<String , Object> result = personService.updateDetails(shipmentId, shipment);
//		return Response.success(result);
//	}
////	
//	@PostMapping("/update/{id}")
//	public ResponseEntity<?> modifyShipmentDetails(@PathVariable("id") int shipmentId , @RequestBody Shipment shipment)
//	{
//		Map<String , Object> result = personService.updateDetails(shipmentId, shipment);
//		return Response.success(result);
//	}
//	@PatchMapping("/blog/{id}/toggle-like")
//	public ResponseEntity<?> toggleBlogLike(@PathVariable("id") int id, @RequestBody UserLikeDTO userLike) {
//		Map<String, Object> result = blogService.saveBlogLike(id, userLike);
//		return Response.success(result);
//	}
	@PatchMapping("/update/{id}")
	public ResponseEntity<?> updateState(@PathVariable("id") int shipmentId, @RequestBody Shipment shipment)
	{
		Map<String, Object> result =personService.updateDetails(shipmentId, shipment);
		return Response.success(result);
	}
	
	
	
	
}
