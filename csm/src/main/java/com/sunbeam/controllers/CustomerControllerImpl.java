//package com.sunbeam.controllers;
//
//import java.util.Map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.sunbeam.dtos.Credentials;
//import com.sunbeam.dtos.PersonDto;
//import com.sunbeam.dtos.Response;
//import com.sunbeam.services.CustomerServiceImpl;
//
//@CrossOrigin
//@RestController
//@RequestMapping("/customer")
//public class CustomerControllerImpl {
//	@Autowired
//	private CustomerServiceImpl customerService;
//
//	@PostMapping("/signin")
//	public ResponseEntity<?> signin(@RequestBody Credentials cred) {
//		PersonDto customerDto = customerService.authenticateCustomer(cred);
//		if (customerDto == null)
//			return Response.error("user not found");
//		return Response.success(customerDto);
//	}
//
//	@PostMapping("/signup")
//	public ResponseEntity<?> signup(@RequestBody PersonDto customerDto) {
//		Map<String, Object> result = customerService.registerCustomer(customerDto);
//		if (result.containsKey("error"))
//			return Response.error(result);
//		return Response.success(result);
//	}
//	
//	
//
//}
