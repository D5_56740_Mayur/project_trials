
package com.sunbeam.dtos;


public class PersonDto {
	private int personId;
	private String name;
	private String email;
	private String phone;
	private String password;
	private String companyName;
	private String role;
	
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public PersonDto(int personId, String name, String email, String phone, String password, String companyName,
			String role) {
		super();
		this.personId = personId;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.password = password;
		this.companyName = companyName;
		this.role = role;
	}
	
	
	public PersonDto() {
		
	}
	
	@Override
	public String toString() {
		return "PersonDto [personId=" + personId + ", name=" + name + ", email=" + email + ", phone=" + phone
				+ ", password=" + password + ", companyName=" + companyName + ", role=" + role + "]";
	}
	
	
}