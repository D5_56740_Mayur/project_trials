package com.sunbeam.dtos;

public class ShipmentDto {
	private int shipmentId;
	
	private String shipmentStatus;
	
	
	public int getShipmentId() {
		return shipmentId;
	}
	public void setShipmentId(int shipmentId) {
		this.shipmentId = shipmentId;
	}

	public String getShipmentStatus() {
		return shipmentStatus;
	}
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}
	

	public ShipmentDto(int shipmentId,  String shipmentStatus) {
		super();
		this.shipmentId = shipmentId;
		
		this.shipmentStatus = shipmentStatus;
	}
	@Override
	public String toString() {
		return "ShipmentDto [shipmentId=" + shipmentId + ", shipmentStatus=" + shipmentStatus + "]";
	}
	
	
	
	
}
