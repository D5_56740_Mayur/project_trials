package com.sunbeam.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table (name = "person")
public class Person {
	
	@Id   
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "person_id")
	private int personId ;
	//*************************************************
//	@OneToOne(mappedBy = "person")
//	private Shipment shipment;
	//*************************************************
	@Column (name = "name")
	private String name;
	
	@Column (name = "company_name")
	private String companyName;
	
	@Column(name = "phone")
	private String phone;
	
	@Column (name = "email")
	private String email;
	
	@Column (name = "password")
	private String password;
	
	@Column(name = "role")
	private String role;
	
	@Column (name = "FK_branch_id")
	private String branchID;
	
	@Column (name = "FK_address_id")
	private String addressID;
	
	
	@Column (name = "account_status")
	private int accountStatus;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column (name = "created_timestamp", insertable = false)
	private Date createdTimestamp;


	public int getPersonId() {
		return personId;
	}


	public void setPersonId(int personId) {
		this.personId = personId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public String getBranchID() {
		return branchID;
	}


	public void setBranchID(String branchID) {
		this.branchID = branchID;
	}


	public String getAddressID() {
		return addressID;
	}


	public void setAddressID(String addressID) {
		this.addressID = addressID;
	}


	public int getAccountStatus() {
		return accountStatus;
	}


	public void setAccountStatus(int accountStatus) {
		this.accountStatus = accountStatus;
	}


	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}


	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}


	public Person(int personId, String name, String companyName, String phone, String email, String password,
			String role, String branchID, String addressID, int accountStatus, Date createdTimestamp) {
		this.personId = personId;
		this.name = name;
		this.companyName = companyName;
		this.phone = phone;
		this.email = email;
		this.password = password;
		this.role = role;
		this.branchID = branchID;
		this.addressID = addressID;
		this.accountStatus = accountStatus;
		this.createdTimestamp = createdTimestamp;
	}


	public Person() {
	}


	@Override
	public String toString() {
		return "Person [personId=" + personId + ", name=" + name + ", companyName=" + companyName + ", phone=" + phone
				+ ", email=" + email + ", password=" + password + ", role=" + role + ", branchID=" + branchID
				+ ", addressID=" + addressID + ", accountStatus=" + accountStatus + ", createdTimestamp="
				+ createdTimestamp + "]";
	}
	
	

	
}

