package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "complaint")
public class Complaint {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "complaint_id")
	private int complaintId;
	@Column
	private String description;
	@Column
	private int ratings;
	@Column
	private String feedback;
	
public Complaint() {
	// TODO Auto-generated constructor stub
}

public Complaint(int complaintId, String description, int ratings, String feedback) {
	super();
	this.complaintId = complaintId;
	this.description = description;
	this.ratings = ratings;
	this.feedback = feedback;
}

public int getComplaintId() {
	return complaintId;
}

public void setComplaintId(int complaintId) {
	this.complaintId = complaintId;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public int getRatings() {
	return ratings;
}

public void setRatings(int ratings) {
	this.ratings = ratings;
}

public String getFeedback() {
	return feedback;
}

public void setFeedback(String feedback) {
	this.feedback = feedback;
}

@Override
public String toString() {
	return "Feedback [complaintId=" + complaintId + ", description=" + description + ", ratings=" + ratings
			+ ", feedback=" + feedback + "]";
}


}
//CREATE TABLE complaint(
//	    complaint_id INT PRIMARY KEY AUTO_INCREMENT,
//	    description VARCHAR(200),
//	    ratings INT,
//	    feedback VARCHAR(200)
//	);