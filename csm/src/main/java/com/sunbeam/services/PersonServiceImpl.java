package com.sunbeam.services;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunbeam.daos.ComplaintDao;
import com.sunbeam.daos.PersonDao;
import com.sunbeam.daos.ShipmentDao;
//import com.sunbeam.daos.ShipmentDao;
//import com.sunbeam.daos.ShipmentDao;
import com.sunbeam.dtos.DtoEntityConverter;
import com.sunbeam.dtos.PersonDto;
import com.sunbeam.entities.Complaint;
import com.sunbeam.entities.Person;
import com.sunbeam.entities.Shipment;
//import com.sunbeam.entities.Shipment;
//import com.sunbeam.entities.Shipment;
//import com.sunbeam.entities.User;
@Transactional
@Service
public class PersonServiceImpl {
	@Autowired
	private PersonDao personDao;
	
	

	@Autowired
	private ComplaintDao complaintDao;

	@Autowired
	private ShipmentDao shipmentDao;
	@Autowired
	private DtoEntityConverter converter;


	public PersonDto findPersonByName(String name) {
		Person person = personDao.findByName(name);
		return converter.toDtoPerson(person);
	}
	

	public PersonDto findPersonById(int id) {
		Person person = personDao.findByPersonId(id);
		return converter.toDtoPerson(person);
	}

	
	public List<Complaint> findAllComplaints()
	
	{
		return complaintDao.findAll();
	}
	
//	public List<Shipment> findShipmentsOfAgent(int id) {
//		return shipmentDao.findAll();
//	}
//	public List<Shipment> findAllShipments(int id)
//	{
//		return shipmentDao.findByperson(id);
//	}
	
//	public Map<String, Object> editDetails(int employeeId, EmployeeDto employeeDto) {
//		if(personDao.existsById(employeeId)) {
//			employeeDto.setPersonId(employeeId); 
//			Person employee = converter.toEmployeeEntity(employeeDto);
//			employee.setName(employeeDto.getName());
//			employee.setRole(employeeDto.getRole());
//			employee.setBranchID(employeeDto.getBranchID());
//			employee.setPhone(employeeDto.getPhone());
//			employee.setEmail(employeeDto.getEmail());
//			employee.setPassword(employeeDto.getPassword());
//			employee = personDao.save(employee);
//			return Collections.singletonMap("changedRows", 1);
//		}
//		return Collections.singletonMap("changedRows", 0);
//	}
	
//	public Map<String, Object> updateDetails(int shipmentId, ShipmentDto shipmentDto)
//	{
//		if(shipmentDao.existsById(shipmentId))
//		{   shipmentDto.setShipmentId(shipmentDto.getShipmentId());
//			shipmentDto.setShipmentStatus("XXXX");
//			Shipment ship=converter.toShipmentEntity(shipmentDto);
//			ship=shipmentDao.save(ship);
//			return Collections.singletonMap("changedRows", 1);
//			
//		}
//		return Collections.singletonMap("changedRows", 0);
//	}
//	
	
	public Map<String, Object> updateDetails(int shipmentId, Shipment shipment)
	{
		
		Shipment s=shipmentDao.findByshipmentId(shipmentId);
		s.setEstimatedDeliveryDate(s.getEstimatedDeliveryDate());
				s.setShipmentStatus("Delivered");
		shipmentDao.save(s);
		return Collections.singletonMap("changedRows", 1);
		
	}
//	public Map<String, Object> saveBlogLike(int blogId, UserLikeDTO userLike) {
//		BlogLikeStatus likeStatus = likeDao.findByBlogIdAndUserId(blogId, userLike.getUserId());
//		if(likeStatus != null) {
//			if(likeStatus.getType() != userLike.getType()) {
//				likeStatus.setType(userLike.getType());
//				likeDao.save(likeStatus);
//				return Collections.singletonMap("changedRows", 1);
//			}
//			return Collections.singletonMap("affectedRows", 0);
//		}
//		else {
//			likeStatus = new BlogLikeStatus(0, userLike.getType(), new Date());
//			likeStatus.setUser(new User(userLike.getUserId()));
//			likeStatus.setBlog(new Blog(blogId));
//			likeStatus = likeDao.save(likeStatus);
//			return Collections.singletonMap("insertedId", likeStatus.getId());
//		}
	
	
	}
	
	

