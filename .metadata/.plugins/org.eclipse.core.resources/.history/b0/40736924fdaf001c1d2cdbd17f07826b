package com.sunbeam.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "shipment")
public class Shipment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "shipment_id")
	private int shipmentId;

	@Column(name = "awb")
	private int awb;

	@Column(name = "shipment_status")
	private String shipmentStatus;

//	@ManyToMany(mappedBy = "shipments")
//	private List<Person> persons;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp", insertable = false)
	private Date createdTimeStamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pickup_date")
	private Date pickUpDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "estimated_delivery_date")
	private Date estimatedDeliveryDate;

	@OneToOne()
	@JoinColumn(name = "FK_service_id")
	private Service service;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_parcel_id")
	private Parcel parcel;
	
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "FK_person_id")
//	private Person person;

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "FK_payment_id")
	private Payment payment;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_complaint_id")
	private Complaint complaint;

	public int getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(int shipmentId) {
		this.shipmentId = shipmentId;
	}

	public int getAwb() {
		return awb;
	}

	public void setAwb(int awb) {
		this.awb = awb;
	}

	public String getShipmentStatus() {
		return shipmentStatus;
	}

	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}

//	public List<Person> getPersons() {
//		return persons;
//	}
//
//	public void setPersons(List<Person> persons) {
//		this.persons = persons;
//	}

	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Service getService() {
		return service;
	}

	public Date getPickUpDate() {
		return pickUpDate;
	}

	public void setPickUpDate(Date pickUpDate) {
		this.pickUpDate = pickUpDate;
	}

	public Date getEstimatedDeliveryDate() {
		return estimatedDeliveryDate;
	}

	public void setEstimatedDeliveryDate(Date estimatedDeliveryDate) {
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Parcel getParcel() {
		return parcel;
	}

	public void setParcel(Parcel parcel) {
		this.parcel = parcel;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Complaint getComplaint() {
		return complaint;
	}

	public void setComplaint(Complaint complaint) {
		this.complaint = complaint;
	}

	@Override
	public String toString() {
		return "Shipment [shipmentId=" + shipmentId + ", awb=" + awb + ", createdTimeStamp="
				+ createdTimeStamp + ", service=" + service + ", parcel=" + parcel + ", payment=" + payment
				+ ", complaint=" + complaint + "]";
	}

}


