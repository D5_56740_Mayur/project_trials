CREATE DATABASE csm1

USE csm1


CREATE TABLE address(
    address_id INT PRIMARY KEY AUTO_INCREMENT,
    address_line1 VARCHAR(200),
    address_line2 VARCHAR(200),
    pin_code INT,
    city_name VARCHAR(50),
    state_name VARCHAR(50),
    country_name VARCHAR(50)
);

insert into  address values(1,"Pitruchaya","near bus-stand",431116,"Aurangabad","MH","India");
insert into  address values(2,"Gokul Dham","VKP school road",431116,"Aurangabad","MH","India");
insert into  address values(3,"Zippy","sunbeam road",431321,"Pune","MH","India");

CREATE TABLE branch (
    branch_id INT PRIMARY KEY AUTO_INCREMENT,
    branch_code VARCHAR(50),
    branch_name VARCHAR(100),
    contact_no VARCHAR(20),
    contact_email VARCHAR(50),
    FK_address_id INT,
    FOREIGN KEY (FK_address_id) REFERENCES address(address_id)
);

insert into  branch values(1,"M1","Zippy Services",7028512954,"zippy@test.com",3);

CREATE TABLE complaint(
    complaint_id INT PRIMARY KEY AUTO_INCREMENT,
    description VARCHAR(200),
    ratings INT,
    feedback VARCHAR(200)
);
insert into  complaint values(1,"Damaged Parcel",1,"Want refund and compensation for my damaged things");


CREATE TABLE parcel(
    parcel_id INT PRIMARY KEY AUTO_INCREMENT,
    parcel_name VARCHAR(50),
    length INT,
    width INT,
    height INT,
    weight INT
);

insert into  parcel values(1,"Box3",4,4,4,4);

CREATE TABLE payment(
    payment_id INT PRIMARY KEY AUTO_INCREMENT,
    payment_type VARCHAR(50),
    card_number VARCHAR(20),
    card_holder_name VARCHAR(50),
    payment_status VARCHAR(50)
);


insert into  payment values(1,"Online","234567","Mayur","Success");


CREATE TABLE service(
    service_id INT PRIMARY KEY AUTO_INCREMENT,
    service_name VARCHAR(50),
    estimated_delivery VARCHAR(50),
    rate DECIMAL
);

insert into  service values(1,"premium","In four days",200);
insert into  service values(2,"standard","In two days",400);

CREATE TABLE person(
    person_id INT PRIMARY KEY AUTO_INCREMENT, 
    name VARCHAR(50), 
    company_name VARCHAR(100),
    phone VARCHAR(20), 
    email VARCHAR(50), 
    password VARCHAR(50), 
    role VARCHAR(30),
    FK_branch_id INT,
    FK_address_id INT,
    FOREIGN KEY(FK_branch_id) REFERENCES branch(branch_id),
    FOREIGN KEY (FK_address_id ) REFERENCES address(address_id),
    account_status BINARY(1) DEFAULT 1,
    created_timestamp DATETIME DEFAULT CURRENT_TIMESTAMP    
);

insert into  person(person_id,name,company_name,phone,email,password,role,FK_branch_id, FK_address_id) values(1,"Pk","Zippy","23455","pk@test.com","1111","admin",1,3);
insert into  person(person_id,name,company_name,phone,email,password,role,FK_branch_id, FK_address_id) values(3,"Mayur","Zippy","14535","mayur@test.com","1111","user",1,1);

CREATE TABLE shipment (
    shipment_id INT PRIMARY KEY AUTO_INCREMENT,
    awb INT,
    shipment_status VARCHAR(50),

    FK_parcel_id INT,
    FK_payment_id INT,
    FK_service_id INT,
    FK_complaint_id INT,

    created_timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
    pickup_date Date,
    estimated_delivery_date Date,

    FOREIGN KEY(FK_parcel_id) REFERENCES parcel(parcel_id),
    FOREIGN KEY(FK_payment_id) REFERENCES payment(payment_id),
    FOREIGN KEY(FK_service_id) REFERENCES service(service_id),
    FOREIGN KEY(FK_complaint_id) REFERENCES complaint(complaint_id)
);

insert into  shipment (shipment_id,awb,shipment_status,FK_parcel_id,FK_payment_id, FK_service_id, pickup_date,estimated_delivery_date) values(1,234,"pending",1,1,1,"2021-12-02","2021-12-04");





CREATE TABLE person_shipment (
    person_shipment_id INT PRIMARY KEY AUTO_INCREMENT,

    FK_person_id INT,
    FK_shipment_id INT,

    FOREIGN KEY(FK_person_id) REFERENCES person(person_id),
    FOREIGN KEY(FK_shipment_id) REFERENCES shipment(shipment_id)  
);

insert into person_shipment values (1,2,1);