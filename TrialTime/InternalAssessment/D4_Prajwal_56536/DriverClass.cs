﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D4_Prajwal_56536
{
    internal class DriverClass
    {
        static void Main(string[] args)
        {
            Dictionary<int, Employee> emps = Populate.employeesDict();
            Dictionary<int, Department> depts = Populate.departmentDict();

            bool flag = true;
 
            while (flag)
            {
               switch(MenuChoice())
                {
                    case 1: // ADD Data
                        switch(AddDataChoice())
                        {
                            case 1:
                                emps=AddEmployee(emps);
                                break;

                            case 2:
                                depts=AddDepartment(depts);
                                break;
                            case 3:
                                 
                                break;
                        }
                        break;
                    case 2:  // Display Data
                        switch(DisplayDataChoice())
                        {
                            case 1:
                                 DisplayEmp(emps);
                                break;

                            case 2:
                                 DisplayDept(depts);
                                break;
                            case 3:
                                 
                                break;
                        }
                        break;

                    case 3:  // Other
                        switch (OtherChoice())
                        {
                            case 1:
                                Calculate_Total_Salary(emps);
                                break;

                            case 2:
                                Console.WriteLine("Enter Department - No : ");
                                int deptNo = Convert.ToInt32(Console.ReadLine());
                                DisplayEmployeeByDepartment(emps, deptNo);
                                break;
                            case 3:
                                 
                                DepartmentWiseCountOfEmployees(emps,depts);
                                break;

                            case 4:
                                DepartmentWiseAverageSalary(emps,depts);
                                break;
                            case 5:
                                DepartmentWiseMinimumSalary(emps, depts);
                                break;
                            case 6:
                                
                                break;

                        }
                        break;
                }
                
            }
           
        }

        static int MenuChoice()
        { 
            
            int choice;
            while (true)
            {
               
                Console.WriteLine("1.Add  2.Display  3.Other  4.Exit ");
                Console.WriteLine("-----------");
                Console.Write("Enter choice : ");
                choice= Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("-----------");
                if (choice==4)
                {  
                   Environment.Exit(0);
                }
                else if(choice==1 || choice ==2 || choice == 3)
                {
                    return choice;
                }
                Console.WriteLine("invalid choice !!");
            }
        }
        static int AddDataChoice()
        {
            
            int choice;
            Console.WriteLine("Tell me What you want to Add !! ");
            Console.WriteLine();
            while (true)
            {

                Console.WriteLine("1.Employee  2.Department 3.back [<-]");
                Console.WriteLine("-----------");
                Console.Write("Enter choice : ");
                choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("-----------");
                if (choice > 0 && choice < 4)
                {
                    return choice;
                }
                Console.WriteLine("invalid choice !!");
            }
        }
        static int DisplayDataChoice()
        {
            Console.WriteLine(" Tell me What you Want to display ? ");
            Console.WriteLine();
            int choice;
            while (true)
            {

                Console.WriteLine("1.Employees  2.Departments 3.back [<-]");
                Console.WriteLine("-----------");
                Console.Write("Enter choice : ");
                choice = Convert.ToInt32(Console.ReadLine());
               
                 if (choice >0 || choice <4 )
                {
                    return choice;
                }
                Console.WriteLine("invalid choice !!");
            }
        }

        static int OtherChoice()
        {
             
            int choice;
            while (true)
            {
 
                Console.WriteLine("1.Total-Salary of All employees");
                Console.WriteLine("2.Employees of particular department");
                Console.WriteLine("3.Department wise count of employees");
                Console.WriteLine("4.Department wise average salary ");
                Console.WriteLine("5.Department wise minimum salary ");
                Console.WriteLine("6.Back to Main MENU ");
                Console.WriteLine("-----------");
                Console.Write("Enter choice : ");
                choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("-----------");
               if (choice >0 && choice < 7)
                {
                    return choice;
                }
                Console.WriteLine("invalid choice !!");
            }
        }
        static Dictionary<int, Employee>  AddEmployee(Dictionary<int,Employee> emps)
        {
            
            Console.Write("Employee No : ");
            int EmpNo = Convert.ToInt32(Console.ReadLine());

            Console.Write("Name : ");
            string Name = Console.ReadLine();

            Console.Write("Designaion : ");
            string Designaion =  Console.ReadLine();

            Console.Write("Salary : ");
            double Salary = Convert.ToDouble(Console.ReadLine());

            Console.Write("Commision : ");
            double Commision = Convert.ToDouble(Console.ReadLine());

            Console.Write("DeptNo : ");
            int DeptNo = Convert.ToInt32(Console.ReadLine());

            emps.Add(EmpNo, new Employee() { EmpNo = EmpNo, Name = Name, Salary = Salary, Commision = Commision, DeptNo = DeptNo, Designation = Designaion });


            return emps;
        }
        static Dictionary<int, Department> AddDepartment(Dictionary<int, Department> depts)
        {
            Console.Write("Department No : ");
            int DeptNo = Convert.ToInt32(Console.ReadLine());

            Console.Write("Dept Name : ");
            string DeptName = Console.ReadLine();

            Console.Write("Location : ");
            string Location = Console.ReadLine();

            depts.Add(DeptNo, new Department() { DeptName = DeptName, DeptNo = DeptNo, Location = Location });

            return depts;
        }

        static void DisplayEmp(Dictionary<int, Employee> emps)
        {
            foreach(Employee emp in emps.Values)
            {
                Console.WriteLine("----------------------------------------------------------");
                Console.WriteLine(emp.ToString());
                Console.WriteLine("----------------------------------------------------------");
            }
        }

        static void DisplayDept(Dictionary<int, Department> depts)
        {
            foreach (Department dept in depts.Values)
            {
                Console.WriteLine("----------------------------------------------------------");
                Console.WriteLine(dept.ToString());
                Console.WriteLine("----------------------------------------------------------");
            }
        }


       static void  Calculate_Total_Salary(Dictionary<int,Employee> emps)
        {
            double TotalSalary=0.0;
            foreach(Employee emp in emps.Values)
            {
                TotalSalary += emp.Salary +emp.Commision;   
            }
            Console.WriteLine("Total Salary Of All Employees is "+TotalSalary +" Rs");
        }

        static void DisplayEmployeeByDepartment(Dictionary<int, Employee> emps,int DeptNo)
        {
          
            var employees = (from emp in emps.Values
                             where emp.DeptNo == DeptNo
                             select emp);
            Console.WriteLine("----------------------------------------------------------------------------------");
            if(employees.Count() ==0)
            {
                Console.WriteLine("No Employee Found With Department No : "+DeptNo);
            }
            else
            {
                Console.WriteLine("All Employess from Department No : " + DeptNo);
                foreach (Employee emp in employees)
                {
                    Console.WriteLine(emp.ToString());
                }
                Console.WriteLine("----------------------------------------------------------------------------------");
            }
            
        }
        static void DepartmentWiseCountOfEmployees(Dictionary<int, Employee> emps, Dictionary<int, Department> depts)
        {


            var result = (from emp in emps.Values join dept in depts.Values
                          on emp.DeptNo equals dept.DeptNo 
                          group emp by dept.DeptName into g
                          select new { Dept = g.Key, Count = g.Count() });
            

            foreach(var data in result)
            {
                Console.WriteLine("Department : "+ data.Dept + " Employee-Count : "+ data.Count);
            }
           

        }
        static void DepartmentWiseAverageSalary(Dictionary<int, Employee> emps, Dictionary<int, Department> depts)
        {

            var result = (from emp in emps.Values join dept in depts.Values
                          on emp.DeptNo equals dept.DeptNo
                          group emp by dept.DeptName into g
                          select new {Dept= g.Key, average_salary = g.Average(x=>x.Salary)});

            foreach(var data in result)
            {
                Console.WriteLine("Department : " + data.Dept + " Average-Salary : " + data.average_salary);
            }
            
        }
        static void DepartmentWiseMinimumSalary(Dictionary<int, Employee> emps, Dictionary<int, Department> depts)
        {

            var result = (from emp in emps.Values join dept in depts.Values
                          on emp.DeptNo equals dept.DeptNo 
                          group emp by dept.DeptName into g
                          select new { Dept = g.Key, minimum_salary = g.Min(x => x.Salary) });


            foreach (var data in result)
            {
                Console.WriteLine("Department : " + data.Dept + " minimum-Salary : " + data.minimum_salary);
            }

        }
    }
    
}
