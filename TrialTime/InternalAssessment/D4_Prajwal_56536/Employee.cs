﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D4_Prajwal_56536
{
    internal class Employee
    {
         
        public int EmpNo { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public double Salary { get; set; }
        public double Commision { get; set; }
        public int DeptNo { get; set; }

        public override string ToString()
        {
            return "EmpNo : " + EmpNo + " | Name : " + Name + " | Designation : " + Designation   + " | Salary : " + Salary + " | Commision : " + Commision + " | DeptNo : " + DeptNo;
        }
    }
}
