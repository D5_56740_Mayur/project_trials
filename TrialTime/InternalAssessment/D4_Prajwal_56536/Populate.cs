﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D4_Prajwal_56536
{
    internal static class Populate
    {
        public static Dictionary<int, Employee> employeesDict()
        {
            Dictionary<int, Employee> emps = new Dictionary<int, Employee>();
            emps.Add(1, new Employee() { EmpNo = 1, Commision = 10.1, Salary = 100, DeptNo = 10, Designation = "engineer", Name = "Prajwal" });
            emps.Add(2, new Employee() { EmpNo = 2, Commision = 10.1, Salary = 50, DeptNo = 10, Designation = "engineer", Name = "Prajwal" });
            emps.Add(3, new Employee() { EmpNo = 3, Commision = 10.1, Salary = 100, DeptNo = 10, Designation = "engineer", Name = "shubham" });
            emps.Add(4, new Employee() { EmpNo = 4, Commision = 10.1, Salary = 100, DeptNo = 30, Designation = "engineer", Name = "ravi" });
            emps.Add(5, new Employee() { EmpNo = 5, Commision = 10.1, Salary = 110, DeptNo = 30, Designation = "engineer", Name = "suresh" });
            emps.Add(6, new Employee() { EmpNo = 6, Commision = 10.1, Salary = 200, DeptNo = 10, Designation = "engineer", Name = "ramesh" });

            return emps;
        }
        public static Dictionary<int, Department> departmentDict()
        {
            Dictionary<int, Department> depts = new Dictionary<int, Department>();
            depts.Add(1, new Department() { DeptName = "IT", DeptNo = 10, Location = "pune" });
            depts.Add(2, new Department() { DeptName = "Computer", DeptNo = 20, Location = "Mumbai" });
            depts.Add(3, new Department() { DeptName = "mechanical", DeptNo = 30, Location = "Lucknow" });
            depts.Add(4, new Department() { DeptName = "Civil", DeptNo = 40, Location = "chennai" });
            
            return depts;
        }

    }
}
