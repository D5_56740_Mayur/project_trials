﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D4_Prajwal_56536
{
    internal class Department
    {

        public int DeptNo { get; set; }
        public string DeptName { get; set; }
        public string Location { get; set; }

        public override string ToString()
        {
            return "Dept No : "+DeptNo +" | DeptName : "+DeptName + " | Location : "+Location;
        }
    }
}
